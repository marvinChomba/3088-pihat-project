# 3088 PiHat project

This is a repo that contains the source files for the project.

The purpose of this project is to design a HAT that will serve as an Uninterrupted Power Supply for the Pi. It will give time for the Pi to shut down in the event of a power outage. 

### Bill of Materials ###

1. 3 510 Ohm resistors.
2. 2 12V batteries.
3. LM2570 IC
